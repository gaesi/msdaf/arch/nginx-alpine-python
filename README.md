## Build (docker)

Run `docker image build -t nginx-alpine-python:1.0 .`. __Remember to update it version!__

## Running (docker)

 Run `docker run -d -p 4200:80 --name ui -it imageID`. IF stuck __or to kill the containr__ use this command to remove container: `docker rm -f containerID`. To access the Docker as BASH: ``$ docker run -it --rm containerID /bin/sh``

## Docker publish

1. Com a imagem ja criada digite o seguinte código no terminal
`docker tag id_docker_hub/nome_da_imagem_hub:tag_version nome_da_imagem`

2. Insira o seguinte código para dar um push para o DockeHub e aguarde
`docker push id_docker_hub/nome_da_imagem_hub:tag_version`
